.. pyBaram documentation master file, created by
   sphinx-quickstart on Wed Jun 29 19:32:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyBaram's documentation!
===================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   install
   user_guide
   examples
   theory
   developer_guide

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
