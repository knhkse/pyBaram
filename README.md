pyBaram
========

Overview
---------
pyBaram is an open-source, Python-based software designed to solve compressible flow using the finite volume method on unstructured grids. 'Baram' translates to 'Wind' in Korean. The software is tailored for solving compressible inviscid flow, laminar flow, and turbulent flow through the use of RANS (Reynolds Averaged Navier-Stokes) models. All the code is written in Python, and hybrid parallel simulations are implemented using high-performance Python packages.

Examples
---------
Examples of using pyBaram are available in the examples directory. Currently available examples includes:

- 3D Inviscid spherical explosion problem

- 2D transonic turbulent flow over RAE2822 airfoil

- 3D transonic turbulent flow over ONERA M6 wing

- 3D supersonic turbulent flow around HB-2 model

Documentation
-------------
Information on the installation, usage, and implementation of pyBaram can be found in the[documentation](https://aadl_inha.gitlab.io/pyBaram/).

Reference
---------
[pyBaram: Parallel compressible flow solver in high-performance Python for teaching and research, SoftwareX, 2022](https://doi.org/10.1016/j.softx.2022.101272)

Authors
--------
See the AUTHORS file.

License
---------
pyBaram is released under the New BSD License.
