Mesh file
---------
You can download the mesh files

* Medium mesh : [link](https://drive.google.com/uc?export=view&id=1eHmRzpTvfuPA7yDowDLUF7QXH8T7RXxO)

* Fine mesh : [link](https://drive.google.com/uc?export=view&id=1uWpPlD7Y230QVqQZt7ka6LekVNF4cubQ)