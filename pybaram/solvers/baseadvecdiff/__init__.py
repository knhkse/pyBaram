# -*- coding: utf-8 -*-
from pybaram.solvers.baseadvecdiff.elements import BaseAdvecDiffElements
from pybaram.solvers.baseadvecdiff.inters import BaseAdvecDiffIntInters, BaseAdvecDiffMPIInters, BaseAdvecDiffBCInters
