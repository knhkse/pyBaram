# -*- coding: utf-8 -*-
from pybaram.plugins.base import BasePlugin
from pybaram.plugins.force import ForcePlugin
from pybaram.plugins.stats import StatsPlugin
from pybaram.plugins.writer import WriterPlugin
from pybaram.plugins.surfint import SurfIntPlugin
from pybaram.utils.misc import subclass_by_name


def get_plugin(name, *args, **kwargs):
    return subclass_by_name(BasePlugin, name)(*args, **kwargs)